const express = require('express');
const cors = require('cors');
const requireDir = require('require-dir');

require('./src/startup/db')();
requireDir('./src/models');

const app = express();

app.use(express.json());
app.use(express.urlencoded());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use(express.static('public'));
app.use('/api', require('./src/startup/routes'));

const port = process.env.PORT || 3000;
const server = app.listen(port, () => console.info(`Listening on port ${port}...`));

module.exports = server;