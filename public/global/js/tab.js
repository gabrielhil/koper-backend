// TABS FEATURES

var global_tabs = [];

clickTab('HOME')

function openTab(url, nomeAba) {

    if (tabAlreadyOpened(url)) {

        clickTab(url);
    } else {

        // Add url to global_tabs
        global_tabs.push(url);

        // cria a aba
        $('<li title="Clique para entrar no programa ' + nomeAba + '" class="liTab" id="liTab-' + url + '">\n\
                <a class="aTab" onclick="clickTab(' + "'" + url + "'" + ')" id="tab-' + url + '">' + nomeAba + '   \n\
                    <i class="icon-cancel-circle2 btnCloseTab" id="btnCloseTab-' + url + '" onclick="closeTab(' + "'" + url + "'" + ')" title="Fechar Programa ' + name + '"></i>\n\
                </a>\n\
            </li>').appendTo('#listTabs');

        setTimeout(() => {
            $(".aTab").ripple();
        }, 100);

        // cria o conteúdo da aba
        $('<div style="height: 100%;" class="tab-pane contentTab" id="contentTab-' + url + '"><iframe class="frameTab" style="width: 100%;" frameBorder="0" id="frameTab-' + url + '"></iframe></div>').appendTo('#listContentTabs');

        // ativa a última aba (criada)
        $('#listTabs a:last').tab('show');
        $(".frameTab").height($(window).height() - 47);
        document.getElementById('frameTab-' + url).src = url;

        clickTab(url);
    }
}

function tabAlreadyOpened(url) {

    if (global_tabs.indexOf(url) == -1) {
        return false;
    } else {
        return true;
    }
}

function clickTab(url) {

    var e = document.getElementById("contentTab-" + url);
    if (e != undefined) {
        var elements = document.getElementsByClassName('contentTab');
        for (var i = 0; i < elements.length; i++) {
            elements[i].classList.remove('active');
        }

        e.classList.add('active');
    }

    //

    var e = document.getElementById("liTab-" + url);
    if (e != undefined) {
        var elements = document.getElementsByClassName('liTab');
        for (var i = 0; i < elements.length; i++) {
            elements[i].classList.remove('active');
        }

        e.classList.add('active');
    }
}

function closeTab(url) {
    document.getElementById("liTab-" + url).remove();
    document.getElementById("contentTab-" + url).remove();

    // remove from global_tabs
    var index = global_tabs.indexOf(url);
    if (index !== -1) global_tabs.splice(index, 1);

    // get last program
    clickTab(global_tabs[0])

    // if global_tags is empty, go to "home" ghost tab
    clickTab("HOME");
}

function lockDragTabs() {

    $("#li_toggleDragTabs").attr("title", "Destravar Abas");
    $("#a_toggleDragTabs").attr("onclick", "unlockDragTabs()");
    $("#a_toggleDragTabs").html("<i class='icon-lock2'></i>")

    $("#listTabs").sortable("disable")
}

function unlockDragTabs() {

    $("#li_toggleDragTabs").attr("title", "Travar Abas");
    $("#a_toggleDragTabs").attr("onclick", "lockDragTabs()");
    $("#a_toggleDragTabs").html("<i class='icon-unlocked2'></i>")

    $("#listTabs").sortable({
        revert: true,
        items: "li",
        change: function (event, ui) {
            ui.helper.addClass("change");
        },
        beforeStop: function (event, ui) {
            ui.helper.removeClass("change");
        }
    });

    $("#listTabs").sortable("option", "disabled", false);
}