$(document).ready(function () {

    breadcrumb();
});

function breadcrumb() {

    // Breadcrumb elements toggler
    // -------------------------

    // Add control button toggler to breadcrumbs if has elements
    $('.breadcrumb-line').has('.breadcrumb-elements').append('<a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>');


    // Toggle visible state of breadcrumb elements
    $('.breadcrumb-elements-toggle').on('click', function () {
        $(this).parent().children('.breadcrumb-elements').toggleClass('visible');
    });
}

function mensagem(titulo, texto, tipo) {

    new PNotify({
        title: titulo,
        text: texto,
        addclass: 'bg-' + tipo
    });
}

function removerMensagens() {

    PNotify.removeAll();
}