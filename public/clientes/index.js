$(document).ready(function () {

    getGrid();
});

function getGrid() {

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            $("#grid").block({
                message: '<div class="text-center">\n\
                            <div class="alert alert-warning bg-vitali alert-rounded">\n\
                                <i class="fa fa-circle-o-notch fa-spin"></i>   <span class="text-semibold">Aguarde</span> enquanto processamos seu pedido.\n\
                            </div>\n\
                        </div>',
                css: { border: '0px solid #a00', backgroundColor: '#fff', color: '#fff' },
                overlayCSS: { backgroundColor: '#fff' },
            });
        },
        language: {
            url: '/global/utils/Portuguese-Brasil.json'
        }
    });

    $('#grid').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: "/api/v1/clientes/table",
            type: "POST"
        },
        columns: [
            {
                data: "_id",
                orderable: true
            },
            {
                data: "nome",
                orderable: true
            },
            {
                data: "cpf",
                orderable: true
            },
            {
                data: "dataNascimento",
                orderable: true
            },
            {
                data: "endereco",
                orderable: true
            },
            {
                data: "renda",
                orderable: true
            }
        ],
        drawCallback: function (settings) {
            setTimeout(() => {
                $(".btn").ripple();
                $(".icon-koper-id").tooltip();
            }, 100);

            $("#grid").unblock();
        },
        searching: false,
        destroy: true,
        bLengthChange: false,
        fixedHeader: true
    });
}

function add() {

    $("#password").val('');
    $("#dialogAddCliente").modal();
}

function save() {

    let command = createCommand();

    if (!validate(command)) {
        return;
    }

    $("#dialogAddCliente_save").attr('disabled', 'disabled');
    $("#dialogAddCliente_save").html("<i class='fa fa-circle-o-notch fa-spin'></i>  Salvando Cliente");
    $("#dialogAddCliente_save").attr("onclick", "");

    $.ajax({
        url: "/api/v1/clientes",
        data: JSON.stringify(command),
        type: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: "json",
        async: true,
        cache: false,
        success: function (response) {
            getGrid();
            $("#dialogAddCliente").modal('hide');
        },
        error: function (response) {

            mensagem('Erro', 'Não foi possível salvar o cliente', 'danger');
        },
        complete: function (response) {
            $("#dialogAddCliente_save").removeAttr('disabled');
            $("#dialogAddCliente_save").html("<i class='icon-cloud-upload'></i>  Salvar Cliente");
            $("#dialogAddCliente_save").attr("onclick", "save()");
        }
    });
}

function validate(command) {

    let valid = true;

    if (command.nome == "") {
        $("#nome").select();
        mensagem('Atenção', 'Insira o nome do cliente', 'danger');
        return false;
    }

    if (command.cpf == "") {
        $("#cpf").select();
        mensagem('Atenção', 'Insira o CPF do cliente', 'danger');
        return false;
    }

    if (command.dataNascimento == "") {
        $("#dataNascimento").select();
        mensagem('Atenção', 'Insira a data de nascimento do cliente', 'danger');
        return false;
    }

    if (command.endereco == "") {
        $("#endereco").select();
        mensagem('Atenção', 'Insira o endereço do cliente', 'danger');
        return false;
    }

    if (command.renda == "") {
        $("#renda").select();
        mensagem('Atenção', 'Insira a renda do cliente', 'danger');
        return false;
    }

    return valid;
}

function createCommand() {

    let command = {};

    command.nome = $("#nome").val().trim();
    command.cpf = $("#cpf").val().trim();
    command.dataNascimento = $("#dataNascimento").val().trim();
    command.endereco = $("#endereco").val().trim();
    command.renda = $("#renda").val().trim();

    return command;
}