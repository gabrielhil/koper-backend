const mongoose = require('mongoose');
const { Venda, validate } = require('../models/Venda');
const { Reserva } = require('../models/Reserva');
const { Unidade } = require('../models/Unidade');

module.exports = {
    async get(req, res) {
        const { page = 1 } = req.query;
        const vendas = await Venda.paginate({}, { page, limit: 20 });
        return res.json(vendas);
    },

    async create(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        let reserva = await Reserva.findById(req.body.reserva);
        let unidades = req.body.unidades;

        if (!unidades) {
            unidades = reserva.unidades;
        }

        // Altera o status de cada unidade da reserva atual que não está presente na nova listagem de unidades
        for (const unidadeId of reserva.unidades.filter(x => !unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            unidade.status = 'disponivel';
            await unidade.save();
        }

        // Verifica o status de cada nova unidade, precisam estar disponíveis para a reserva
        for (const unidadeId of unidades.filter(x => !reserva.unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            if (unidade.status != 'disponivel') {
                const error = new Error(`A unidade '${unidadeId}' não se encontra disponível para reserva.`);
                return res.json(error.message);
            }
        }

        let valorTotal = 0;
        for (const unidadeId of unidades.filter(x => !reserva.unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            valorTotal += unidade.valor;

            unidade.status = 'reservada';
            await unidade.save();
        }

        reserva = await Reserva.findByIdAndUpdate(req.body.reserva,
            {
                unidades: unidades,
                valorTotal: valorTotal
            }, { new: true }
        );

        const venda = new Venda({
            reserva: req.body.reserva,
            cliente: req.body.cliente,
            dataVenda: new Date(),
            unidades: unidades,
            formasDePagamento: req.body.formasDePagamento,
            status: 'analise'
        });

        await venda.save();
        return res.json(venda);
    },

    async getById(req, res) {
        const venda = await Venda.findById(req.params.id);
        return res.json(venda);
    },
};