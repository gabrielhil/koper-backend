const mongoose = require('mongoose');
const { Pavimento, validate } = require('../models/Pavimento');

module.exports = {
    async get(req, res) {
        const { page = 1 } = req.query;
        const pavimentos = await Pavimento.paginate({}, { page, limit: 20 });
        return res.json(pavimentos);
    },

    async create(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const pavimento = new Pavimento({
            nome: req.body.nome,
            posicao: req.body.posicao,
            bloco: req.body.bloco
        });

        await pavimento.save();
        return res.json(pavimento);
    },

    async getById(req, res) {
        const pavimento = await Pavimento.findById(req.params.id);
        return res.json(pavimento);
    },

    async update(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const pavimento = await Pavimento.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(pavimento);
    },

    async delete(req, res) {
        await Pavimento.findByIdAndRemove(req.params.id);
        return res.send();
    }
};