const mongoose = require('mongoose');
const { Cliente, validate } = require('../models/Cliente');

module.exports = {
    async get(req, res) {
        const { page = 1 } = req.query;
        const clientes = await Cliente.paginate({}, { page, limit: 20 });
        return res.json(clientes);
    },

    async create(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const cliente = new Cliente({
            nome: req.body.nome,
            cpf: req.body.cpf,
            dataNascimento: req.body.dataNascimento,
            endereco: req.body.endereco,
            renda: req.body.renda
        });

        await cliente.save();
        return res.json(cliente);
    },

    async getById(req, res) {
        const cliente = await Cliente.findById(req.params.id);
        return res.json(cliente);
    },

    async update(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const cliente = await Cliente.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(cliente);
    },

    async delete(req, res) {
        await Cliente.findByIdAndRemove(req.params.id);
        return res.send();
    },

    async table(req, res) {

        const fields = ['_id', 'nome', 'cpf', 'dataNascimento', 'endereco', 'renda'];
        Cliente.dataTables({
            limit: req.body.length,
            skip: req.body.start,
            search: {
                value: req.body.search.value,
                fields: fields
            },
            sort: {
                [fields[req.body.order[0]['column']]]: req.body.order[0]['dir'] == 'desc' ? -1 : 1
            }
        }).then(function (table) {
            res.json(table);
        }).catch(function (error) {
            console.error(error);
        });
    }
};