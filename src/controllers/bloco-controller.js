const mongoose = require('mongoose');
const { Bloco, validate } = require('../models/Bloco');

module.exports = {
    async get(req, res) {
        const { page = 1 } = req.query;
        const blocos = await Bloco.paginate({}, { page, limit: 20 });
        return res.json(blocos);
    },

    async create(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const bloco = new Bloco({
            nome: req.body.nome,
            empreendimento: req.body.empreendimento
        });

        await bloco.save();
        return res.json(bloco);
    },

    async getById(req, res) {
        const bloco = await Bloco.findById(req.params.id);
        return res.json(bloco);
    },

    async update(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const bloco = await Bloco.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(bloco);
    },

    async delete(req, res) {
        await Bloco.findByIdAndRemove(req.params.id);
        return res.send();
    }
};