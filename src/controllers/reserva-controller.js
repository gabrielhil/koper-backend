const mongoose = require('mongoose');
const { Reserva, validate } = require('../models/Reserva');
const { Unidade } = require('../models/Unidade');
const { Proposta } = require('../models/Proposta');

module.exports = {
    async get(req, res) {
        const { page = 1 } = req.query;
        const reservas = await Reserva.paginate({}, { page, limit: 20 });
        return res.json(reservas);
    },

    async create(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        // Verifica o status de cada unidade, precisam estar disponíveis para a reserva
        for (const unidadeId of req.body.unidades) {
            const unidade = await Unidade.findById(unidadeId);
            if (unidade.status != 'disponivel') {
                const error = new Error(`A unidade '${unidadeId}' não se encontra disponível para reserva.`);
                return res.json(error.message);
            }
        }

        let valorTotal = 0;
        for (const unidadeId of req.body.unidades) {
            const unidade = await Unidade.findById(unidadeId);
            valorTotal += unidade.valor;

            unidade.status = 'reservada';
            await unidade.save();
        }

        const reserva = new Reserva({
            cliente: req.body.cliente,
            dataReserva: new Date(),
            unidades: req.body.unidades,
            valorTotal: valorTotal
        });

        await reserva.save();
        return res.json(reserva);
    },

    async getById(req, res) {
        const reserva = await Reserva.findById(req.params.id);
        return res.json(reserva);
    },

    async update(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        // Procura por alguma proposta com o status 'aprovada' ou 'analise'
        const proposta = await Proposta.findOne({ 'reserva': req.params.id, 'status': { $in: ['aprovada', 'analise'] } });
        if (proposta) {
            const error = new Error(`A reserva '${req.params.id}' não pode ser editada pois já está em uso pela proposta '${proposta._id}'.`);
            return res.json(error.message);
        }

        let reserva = await Reserva.findById(req.params.id);

        // Altera o status de cada unidade da reserva atual que não está presente na nova listagem de unidades
        for (const unidadeId of reserva.unidades.filter(x => !req.body.unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            unidade.status = 'disponivel';
            await unidade.save();
        }

        // Verifica o status de cada nova unidade, precisam estar disponíveis para a reserva
        for (const unidadeId of req.body.unidades.filter(x => !reserva.unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            if (unidade.status != 'disponivel') {
                const error = new Error(`A unidade '${unidadeId}' não se encontra disponível para reserva.`);
                return res.json(error.message);
            }
        }

        let valorTotal = 0;
        for (const unidadeId of req.body.unidades.filter(x => !reserva.unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            valorTotal += unidade.valor;

            unidade.status = 'reservada';
            await unidade.save();
        }

        reserva = await Reserva.findByIdAndUpdate(req.params.id,
            {
                unidades: req.body.unidades,
                valorTotal: valorTotal
            }, { new: true });
        return res.json(reserva);
    },

    async cancel(req, res) {
        let reserva = await Reserva.findById(req.params.id);

        for (const unidadeId of reserva.unidades) {
            const unidade = await Unidade.findById(unidadeId);
            unidade.status = 'disponivel';
            await unidade.save();
        }

        reserva = await Reserva.findByIdAndUpdate(req.params.id,
            {
                dataCancelamento: new Date()
            }, { new: true });
        return res.json(reserva);
    },

    async delete(req, res) {
        let reserva = await Reserva.findById(req.params.id);

        for (const unidadeId of reserva.unidades) {
            const unidade = await Unidade.findById(unidadeId);
            unidade.status = 'disponivel';
            await unidade.save();
        }

        reserva = await Reserva.findByIdAndRemove(req.params.id);
        return res.json(reserva);
    }
};