const mongoose = require('mongoose');
const { Proposta, validate } = require('../models/Proposta');
const { Reserva } = require('../models/Reserva');
const { Unidade } = require('../models/Unidade');

module.exports = {
    async get(req, res) {
        const { page = 1 } = req.query;
        const propostas = await Proposta.paginate({}, { page, limit: 20 });
        return res.json(propostas);
    },

    async create(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        let reserva = await Reserva.findById(req.body.reserva);
        let unidades = req.body.unidades;

        if (!unidades) {
            unidades = reserva.unidades;
        }

        // Altera o status de cada unidade da reserva atual que não está presente na nova listagem de unidades
        for (const unidadeId of reserva.unidades.filter(x => !unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            unidade.status = 'disponivel';
            await unidade.save();
        }

        // Verifica o status de cada nova unidade, precisam estar disponíveis para a reserva
        for (const unidadeId of unidades.filter(x => !reserva.unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            if (unidade.status != 'disponivel') {
                const error = new Error(`A unidade '${unidadeId}' não se encontra disponível para reserva.`);
                return res.json(error.message);
            }
        }

        let valorTotal = 0;
        for (const unidadeId of unidades.filter(x => !reserva.unidades.includes(x))) {
            const unidade = await Unidade.findById(unidadeId);
            valorTotal += unidade.valor;

            unidade.status = 'reservada';
            await unidade.save();
        }

        reserva = await Reserva.findByIdAndUpdate(req.body.reserva,
            {
                unidades: unidades,
                valorTotal: valorTotal
            }, { new: true }
        );

        const proposta = new Proposta({
            reserva: req.body.reserva,
            cliente: req.body.cliente,
            dataProposta: new Date(),
            unidades: unidades,
            formasDePagamento: req.body.formasDePagamento,
            status: 'analise'
        });

        await proposta.save();
        return res.json(proposta);
    },

    async getById(req, res) {
        const proposta = await Proposta.findById(req.params.id);
        return res.json(proposta);
    },

    async update(req, res) {

    },

    async cancel(req, res) {
        // let proposta = await Proposta.findById(req.params.id);

        // for (const unidadeId of proposta.unidades) {
        //     const unidade = await Unidade.findById(unidadeId);
        //     unidade.status = 'disponivel';
        //     await unidade.save();
        // }

        // proposta = await Proposta.findByIdAndUpdate(req.params.id,
        //     {
        //         dataCancelamento: new Date()
        //     }, { new: true });
        // return res.json(proposta);
    },

    async approve(req, res) {
        const proposta = await Proposta.findByIdAndUpdate(req.params.id,
            {
                status: 'aprovada',
                dataAprovacao: new Date()
            }, { new: true });
        return res.json(proposta);
    },

    async delete(req, res) {
        // let proposta = await Proposta.findById(req.params.id);

        // for (const unidadeId of proposta.unidades) {
        //     const unidade = await Unidade.findById(unidadeId);
        //     unidade.status = 'disponivel';
        //     await unidade.save();
        // }

        // proposta = await Proposta.findByIdAndRemove(req.params.id);
        // return res.json(proposta);
    }
};