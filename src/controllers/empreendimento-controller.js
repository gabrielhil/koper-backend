const mongoose = require('mongoose');
const { Empreendimento, validate } = require('../models/Empreendimento');

module.exports = {
    async get(req, res) {
        const { page = 1 } = req.query;
        const empreendimentos = await Empreendimento.paginate({}, { page, limit: 20 });
        return res.json(empreendimentos);
    },

    async create(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const empreendimento = new Empreendimento({
            nome: req.body.nome,
            dataInicio: req.body.dataInicio,
            dataConclusao: req.body.dataConclusao,
            tipoEmpreendimento: req.body.tipoEmpreendimento,
            endereco: req.body.endereco,
            areaTotal: req.body.areaTotal
        });

        await empreendimento.save();
        return res.json(empreendimento);
    },

    async getById(req, res) {
        const empreendimento = await Empreendimento.findById(req.params.id);
        return res.json(empreendimento);
    },

    async update(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const empreendimento = await Empreendimento.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(empreendimento);
    },

    async delete(req, res) {
        await Empreendimento.findByIdAndRemove(req.params.id);
        return res.send();
    }
};