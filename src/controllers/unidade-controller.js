const mongoose = require('mongoose');
const { Unidade, validate } = require('../models/Unidade');

module.exports = {
    async get(req, res) {
        const { page = 1 } = req.query;
        const unidades = await Unidade.paginate({}, { page, limit: 20 });
        return res.json(unidades);
    },

    async create(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const unidade = new Unidade({
            nome: req.body.nome,
            status: req.body.status,
            areaPrivativa: req.body.areaPrivativa,
            areaComum: req.body.areaComum,
            vendavel: req.body.vendavel,
            valor: req.body.valor,
            valorMinimoDeVenda: req.body.valorMinimoDeVenda,
            empreendimento: req.body.empreendimento,
            bloco: req.body.bloco,
            pavimento: req.body.pavimento
        });
        await unidade.save();
        return res.json(unidade);
    },

    async getById(req, res) {
        const unidade = await Unidade.findById(req.params.id);
        return res.json(unidade);
    },

    async update(req, res) {
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        const unidade = await Unidade.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(unidade);
    },

    async delete(req, res) {
        await Unidade.findByIdAndRemove(req.params.id);
        return res.send();
    }
};