const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');

// Load Mongoose Plugins
const mongoosePaginate = require('mongoose-paginate');
const dataTables = require('gabriel-hildebrandt-mongoose-datatables');

const PropostaSchema = new mongoose.Schema(
    {
        reserva: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Reserva'
        },
        cliente: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Cliente'
        },
        dataProposta: Date,
        dataAprovacao: Date,
        status: {
            type: String,
            enum: ['aprovada', 'analise', 'cancelada', 'rejeitada']
        },
        unidades: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Unidade'
        }],
        formasDePagamento: String
    },
    {
        timestamps: true
    }
);

function validateProposta(proposta) {
    const schema = {
        reserva: Joi.objectId().required(),
        cliente: Joi.objectId().required(),
        dataProposta: Joi.date(),
        dataAprovacao: Joi.date(),
        status: Joi.string().valid('aprovada', 'analise', 'cancelada', 'rejeitada'),
        unidades: Joi.array().items(Joi.objectId()),
        formasDePagamento: Joi.string()
    };

    return Joi.validate(proposta, schema);
}

// Configure Mongoose Plugins
PropostaSchema.plugin(mongoosePaginate);
PropostaSchema.plugin(dataTables);

const Proposta = mongoose.model('Proposta', PropostaSchema);

exports.Proposta = Proposta;
exports.validate = validateProposta;