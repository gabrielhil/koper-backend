const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');

// Load Mongoose Plugins
const mongoosePaginate = require('mongoose-paginate');
const dataTables = require('gabriel-hildebrandt-mongoose-datatables');

const VendaSchema = new mongoose.Schema(
    {
        cliente: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Cliente'
        },
        dataVenda: Date,
        unidades: [
            {
                unidade: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'Unidade'
                },
                valor: Number
            }
        ],
        valorTotal: Number,
        formasDePagamento: String
    },
    {
        timestamps: true
    }
);

function validateVenda(venda) {

    const unidadeSchema = Joi.object({
        unidade: Joi.objectId().required(),
        valor: Joi.number().required()
    }).required();

    const schema = {
        cliente: Joi.objectId().required(),
        dataVenda: Joi.date(),
        unidades: Joi.array().items(unidadeSchema).min(1),
        valorTotal: Joi.number(),
        formasDePagamento: Joi.string()
    };

    return Joi.validate(venda, schema);
}

// Configure Mongoose Plugins
VendaSchema.plugin(mongoosePaginate);
VendaSchema.plugin(dataTables);

const Venda = mongoose.model('Venda', VendaSchema);

exports.Venda = Venda;
exports.validate = validateVenda;