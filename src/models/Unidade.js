const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');

// Load Mongoose Plugins
const mongoosePaginate = require('mongoose-paginate');
const dataTables = require('gabriel-hildebrandt-mongoose-datatables');

const UnidadeSchema = new mongoose.Schema(
    {
        nome: {
            type: String,
            required: true,
            maxlength: 200
        },
        status: {
            type: String,
            enum: ['disponivel', 'indisponivel', 'reservada', 'vendida']
        },
        areaPrivativa: Number,
        areaComum: Number,
        vendavel: Boolean,
        valor: Number,
        valorMinimoDeVenda: Number,
        empreendimento: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Empreendimento'
        },
        bloco: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Bloco'
        },
        pavimento: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Pavimento'
        }
    },
    {
        timestamps: true
    }
);

function validateUnidade(unidade) {
    const schema = {
        nome: Joi.string().max(200).required(),
        status: Joi.string().valid('disponivel', 'indisponivel', 'reservada', 'vendida'),
        areaPrivativa: Joi.number(),
        areaComum: Joi.number(),
        vendavel: Joi.boolean(),
        valor: Joi.number(),
        valorMinimoDeVenda: Joi.number(),
        empreendimento: Joi.objectId(),
        bloco: Joi.objectId(),
        pavimento: Joi.objectId()
    };

    return Joi.validate(unidade, schema);
}

// Configure Mongoose Plugins
UnidadeSchema.plugin(mongoosePaginate);
UnidadeSchema.plugin(dataTables);

const Unidade = mongoose.model('Unidade', UnidadeSchema);

exports.Unidade = Unidade;
exports.validate = validateUnidade;