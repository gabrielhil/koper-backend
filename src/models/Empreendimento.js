const Joi = require('joi');
const mongoose = require('mongoose');

// Load Mongoose Plugins
const mongoosePaginate = require('mongoose-paginate');
const dataTables = require('gabriel-hildebrandt-mongoose-datatables');

const EmpreendimentoSchema = new mongoose.Schema(
    {
        nome: {
            type: String,
            required: true,
            maxlength: 200
        },
        dataInicio: Date,
        dataConclusao: Date,
        tipoEmpreendimento: {
            type: String,
            enum: ['comercial', 'residencial', 'comercial-e-residencial', 'loteamento']
        },
        endereco: {
            type: String,
            maxlength: 200
        },
        areaTotal: Number
    },
    {
        timestamps: true
    }
);

function validateEmpreendimento(empreendimento) {
    const schema = {
        nome: Joi.string().max(200).required(),
        dataInicio: Joi.date(),
        dataConclusao: Joi.date(),
        tipoEmpreendimento: Joi.string().valid('comercial', 'residencial', 'comercial-e-residencial', 'loteamento'),
        endereco: Joi.string().max(200),
        areaTotal: Joi.number()
    };

    return Joi.validate(empreendimento, schema);
}

// Configure Mongoose Plugins
EmpreendimentoSchema.plugin(mongoosePaginate);
EmpreendimentoSchema.plugin(dataTables);

const Empreendimento = mongoose.model('Empreendimento', EmpreendimentoSchema);

exports.Empreendimento = Empreendimento;
exports.validate = validateEmpreendimento;