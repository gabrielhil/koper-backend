const Joi = require('joi');
const mongoose = require('mongoose');

// Load Mongoose Plugins
const mongoosePaginate = require('mongoose-paginate');
const dataTables = require('gabriel-hildebrandt-mongoose-datatables');

const ClienteSchema = new mongoose.Schema(
    {
        nome: {
            type: String,
            required: true,
            maxlength: 200
        },
        cpf: {
            type: String,
            maxlength: 11
        },
        dataNascimento: Date,
        endereco: {
            type: String,
            maxlength: 200
        },
        renda: Number
    },
    {
        timestamps: true
    }
);

function validateCliente(cliente) {
    const schema = {
        nome: Joi.string().max(200).required(),
        cpf: Joi.string().max(11),
        dataNascimento: Joi.date(),
        endereco: Joi.string().max(200),
        renda: Joi.number()
    };

    return Joi.validate(cliente, schema);
}

// Configure Mongoose Plugins
ClienteSchema.plugin(mongoosePaginate);
ClienteSchema.plugin(dataTables);

const Cliente = mongoose.model('Cliente', ClienteSchema);

exports.Cliente = Cliente;
exports.validate = validateCliente;