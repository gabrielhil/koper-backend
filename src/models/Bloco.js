const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');

// Load Mongoose Plugins
const mongoosePaginate = require('mongoose-paginate');
const dataTables = require('gabriel-hildebrandt-mongoose-datatables');

const BlocoSchema = new mongoose.Schema(
    {
        nome: {
            type: String,
            required: true,
            maxlength: 200
        },
        empreendimento: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Empreendimento'
        }
    },
    {
        timestamps: true
    }
);

function validateBloco(bloco) {
    const schema = {
        nome: Joi.string().max(200).required(),
        empreendimento: Joi.objectId()
    };

    return Joi.validate(bloco, schema);
}

// Configure Mongoose Plugins
BlocoSchema.plugin(mongoosePaginate);
BlocoSchema.plugin(dataTables);

const Bloco = mongoose.model('Bloco', BlocoSchema);

exports.Bloco = Bloco;
exports.validate = validateBloco;