const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');

// Load Mongoose Plugins
const mongoosePaginate = require('mongoose-paginate');
const dataTables = require('gabriel-hildebrandt-mongoose-datatables');

const PavimentoSchema = new mongoose.Schema(
    {
        nome: {
            type: String,
            required: true,
            maxlength: 200
        },
        posicao: Number,
        bloco: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Bloco'
        }
    },
    {
        timestamps: true
    }
);

function validatePavimento(pavimento) {
    const schema = {
        nome: Joi.string().max(200).required(),
        posicao: Joi.number(),
        bloco: Joi.objectId()
    };

    return Joi.validate(pavimento, schema);
}

// Configure Mongoose Plugins
PavimentoSchema.plugin(mongoosePaginate);
PavimentoSchema.plugin(dataTables);

const Pavimento = mongoose.model('Pavimento', PavimentoSchema);

exports.Pavimento = Pavimento;
exports.validate = validatePavimento;