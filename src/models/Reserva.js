const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');

// Load Mongoose Plugins
const mongoosePaginate = require('mongoose-paginate');
const dataTables = require('gabriel-hildebrandt-mongoose-datatables');

const ReservaSchema = new mongoose.Schema(
    {
        cliente: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Cliente'
        },
        dataReserva: Date,
        dataCancelamento: Date,
        unidades: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Unidade'
        }],
        valorTotal: Number
    },
    {
        timestamps: true
    }
);

function validateReserva(reserva) {
    const schema = {
        cliente: Joi.objectId(),
        dataReserva: Joi.date(),
        dataCancelamento: Joi.date(),
        unidades: Joi.array().items(Joi.objectId()).min(1),
        valorTotal: Joi.number()
    };

    return Joi.validate(reserva, schema);
}

// Configure Mongoose Plugins
ReservaSchema.plugin(mongoosePaginate);
ReservaSchema.plugin(dataTables);

const Reserva = mongoose.model('Reserva', ReservaSchema);

exports.Reserva = Reserva;
exports.validate = validateReserva;