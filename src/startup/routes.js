const express = require('express');
const clienteController = require('../controllers/cliente-controller');
const empreendimentoController = require('../controllers/empreendimento-controller');
const blocoController = require('../controllers/bloco-controller');
const pavimentocontroller = require('../controllers/pavimento-controller');
const unidadeController = require('../controllers/unidade-controller');
const reservaController = require('../controllers/reserva-controller');
const propostaController = require('../controllers/proposta-controller');
const vendaController = require('../controllers/venda-controller');

const routes = express.Router();

// Clientes
routes.get('/v1/clientes', clienteController.get);
routes.post('/v1/clientes', clienteController.create);
routes.get('/v1/clientes/:id', clienteController.getById);
routes.patch('/v1/clientes/:id', clienteController.update);
routes.delete('/v1/clientes/:id', clienteController.delete);
routes.post('/v1/clientes/table', clienteController.table);

// Empreendimentos
routes.get('/v1/empreendimentos', empreendimentoController.get);
routes.post('/v1/empreendimentos', empreendimentoController.create);
routes.get('/v1/empreendimentos/:id', empreendimentoController.getById);
routes.patch('/v1/empreendimentos/:id', empreendimentoController.update);
routes.delete('/v1/empreendimentos/:id', empreendimentoController.delete);

// Blocos
routes.get('/v1/blocos', blocoController.get);
routes.post('/v1/blocos', blocoController.create);
routes.get('/v1/blocos/:id', blocoController.getById);
routes.patch('/v1/blocos/:id', blocoController.update);
routes.delete('/v1/blocos/:id', blocoController.delete);

// Pavimentos
routes.get('/v1/pavimentos', pavimentocontroller.get);
routes.post('/v1/pavimentos', pavimentocontroller.create);
routes.get('/v1/pavimentos/:id', pavimentocontroller.getById);
routes.patch('/v1/pavimentos/:id', pavimentocontroller.update);
routes.delete('/v1/pavimentos/:id', pavimentocontroller.delete);

// Unidades
routes.get('/v1/unidades', unidadeController.get);
routes.post('/v1/unidades', unidadeController.create);
routes.get('/v1/unidades/:id', unidadeController.getById);
routes.patch('/v1/unidades/:id', unidadeController.update);
routes.delete('/v1/unidades/:id', unidadeController.delete);

// Reservas
routes.get('/v1/reservas', reservaController.get);
routes.post('/v1/reservas', reservaController.create);
routes.get('/v1/reservas/:id', reservaController.getById);
routes.patch('/v1/reservas/:id', reservaController.update);
routes.delete('/v1/reservas/:id', reservaController.delete);
routes.post('/v1/reservas/:id/cancel', reservaController.cancel);

// Propostas
routes.get('/v1/propostas', propostaController.get);
routes.post('/v1/propostas', propostaController.create);
routes.get('/v1/propostas/:id', propostaController.getById);
routes.patch('/v1/propostas/:id', propostaController.update);
routes.delete('/v1/propostas/:id', propostaController.delete);
routes.post('/v1/propostas/:id/cancel', propostaController.cancel);
routes.post('/v1/propostas/:id/approve', propostaController.approve);

// Vendas
routes.get('/v1/vendas', vendaController.get);
routes.post('/v1/vendas', vendaController.create);
routes.get('/v1/vendas/:id', vendaController.getById);

module.exports = routes;